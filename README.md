# Arch Linux Install Script #

### What is this repository for? ###

This is in install script for arch linux, meant for personal use. Mount future root partition to /mnt, and mount all other subdirectories (/home, /boot) in /mnt. Clone the repository and run arch-deploy.sh. It will install the minimal working environment (basic x server, wifi, terminal, git).